package ch.bbw;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * This class is the main class of the "World of Zuul" application. "World of
 * Zuul" is a very simple, text based adventure game. Users can walk around some
 * scenery. That's all. It should really be extended to make it more
 * interesting!
 * 
 * To play this game, create an instance of this class and call the "play"
 * method.
 * 
 * This main class creates and initialises all the others: it creates all rooms,
 * creates the parser and starts the game. It also evaluates and executes the
 * commands that the parser returns.
 * 
 * @author jfr
 * @version 2.0 (Oktober 2018)
 */

public class Game {
	private Parser parser;
	private Room currentRoom;
	
	private boolean wantToQuit;
	private Inventar invent = new Inventar();


	/**
	 * Create the game and initialise its internal map.
	 */
	public Game() {
		createRooms();
		parser = new Parser();
	}

	/**
	 * Create all the rooms and link their exits together.
	 */
	private void createRooms() {
		// create the rooms

		ArrayList<Room> room = new ArrayList<>();

		//Erstellen der R�ume
		Room eingang = new Room("am Eingang");
		Room labor = new Room("im Labor");
		Room wohnzimmer = new Room("im Wohnzimmer");
		Room zimmer = new Room("im Zimmer");
		Room k�che = new Room("in der K�che");
		Room garten = new Room("im Garten");

		//Zum Raumarray hinzuf�gen
		room.add(eingang);
		room.add(labor);
		room.add(wohnzimmer);
		room.add(zimmer);
		room.add(k�che);
		room.add(garten);
		
		//Erstellen der Exits
		eingang.addExit(new Exit("Wohn", wohnzimmer));

		labor.addExit(new Exit("Eingang", eingang));
		labor.addExit(new Exit("Wohn", wohnzimmer));

		wohnzimmer.addExit(new Exit("Labor", labor));
		wohnzimmer.addExit(new Exit("Eingang", eingang));
		wohnzimmer.addExit(new Exit("Kueche", k�che));
		wohnzimmer.addExit(new Exit("Garten", garten));
		wohnzimmer.addExit(new Exit("Zimmer", zimmer));
		
		zimmer.addExit(new Exit("Wohn", wohnzimmer));
		zimmer.addExit(new Exit());
		zimmer.addItem(new Item("Schl�ssel"));

		k�che.addExit(new Exit("Wohn", wohnzimmer));
		k�che.addExit(new Exit("Eingang", eingang));
		k�che.addExit(new Exit("Eingang", eingang));
		k�che.addExit(new Exit("Keller", labor));

		garten.addExit(new Exit("Wohn", wohnzimmer));
		garten.addItem(new Item("Schaufel"));
		
		//Startraum
		currentRoom = eingang; // start game outside
	}

	/**
	 * Main play routine. Loops until end of play.
	 */
	public void play() {
		printWelcome();

		// Enter the main command loop. Here we repeatedly read commands and
		// execute them until the game is over.

		boolean finished = false;
		while (!finished) {
			Command command = parser.getCommand();
			finished = processCommand(command);
		}
		System.out.println("Thank you for playing.  Good bye.");
	}

	/**
	 * Print out the opening message for the player.
	 */
	private void printWelcome() {
		System.out.println();
		System.out.println("Welcome to Adventure!");
		System.out.println("Adventure is a new, incredibly boring adventure game.");
		System.out.println("Type 'help' if you need help.");
		System.out.println();
		System.out.println("You are " + currentRoom.getDescription());
		System.out.println("if you Take a Exit with not exist you have lost.");
		System.out.print("Exits: ");

		for (int i = 0; i < currentRoom.exits.size(); i++) {
			System.out.print(currentRoom.exits.get(i).getName());
		}
	}

	/**
	 * Given a command, process (that is: execute) the command. If this command ends
	 * the game, true is returned, otherwise false is returned.
	 * 
	 * @param command der eingegebene Befehl
	 * @return gibt zur�ck ob man das Spiel verlassen will
	 */
	private boolean processCommand(Command command) {
		wantToQuit = false;

		if (command.isUnknown()) {
			System.out.println("I don't know what you mean...");
			return false;
		}

		String commandWord = command.getCommandWord();
		if (commandWord.equals("help"))
			printHelp();
		else if (commandWord.equals("go"))
			goRoom(command);
		else if (commandWord.equals("quit"))
			wantToQuit = quit(command);

		return wantToQuit;
	}

	// implementations of user commands:

	/**
	 * Print out some help information. Here we print some stupid, cryptic message
	 * and a list of the command words.
	 */
	private void printHelp() {
		System.out.println("You are lost. You are alone. You wander");
		System.out.println("around at the university.");
		System.out.println();
		System.out.println("Your command words are:");
		System.out.println("   go quit help");
	}

	/**
	 * Try to go to one direction. If there is an exit, enter the new room,
	 * otherwise print an error message.
	 * 
	 * @param command der eingegebene Befehl
	 */
	private void goRoom(Command command) {
		boolean win = false;
		int i = 0;
		ArrayList<String> allExits = new ArrayList<>();
		Item schluessel = new Item("Schl�ssel");
		Item schaufel = new Item("Schaufel");

		for (int j = 0; j < currentRoom.exits.size(); j++) {
			allExits.add(currentRoom.exits.get(j).getName());
		}

		if (!command.hasSecondWord()) {
			// if there is no second word, we don't know where to go...
			System.out.println("Go where?");
			return;
		}

		String direction = command.getSecondWord();

		// Try to leave current room.
		Room nextRoom = null;

		for (i = 0; i < currentRoom.exits.size(); i++) {
			
			//false command
			if (!allExits.contains(command.getSecondWord())) {
				System.out.println("Keine g�ltige T�r, Geben Sie den Kommand nochmals an.");
				System.out.print("Exits: ");

				for (i = 0; i < currentRoom.exits.size(); i++) {
					System.out.print(currentRoom.exits.get(i).getName() + ", ");
				}
				return;
			}
			
			//aus der Map gefallen
			if (direction.equals("Bad") && direction.equals(currentRoom.exits.get(i).getName())) {
				System.out.println("Sie sind ins WC gefallen.");
				wantToQuit = true;
				return;
			} else if(direction.equals("Keller") && direction.equals(currentRoom.exits.get(i).getName())) {
				// Nachricht dass Schaufel ben�tigt wird
				if(invent.getItems().contains(schaufel)) {
					nextRoom = currentRoom.exits.get(i).getNextRoom();
				} else {
					System.out.println("Schaufel wird ben�tigt");
					nextRoom = currentRoom;
					return;
				}
			} else if (direction.equals(currentRoom.exits.get(i).getName())) {
				nextRoom = currentRoom.exits.get(i).getNextRoom();
			}
		}
		
		//Schaut ob er die Kodierung erreicht hat
		if (currentRoom.getDescription().equals( "in der K�che")) {
			win = true;
		} else {
			win = false;
		}
		
		//Item wird in den Inventargelegt
		if(currentRoom.getDescription().equals("im Zimmer")){
			invent.addItem(schluessel);
		} 
		if (currentRoom.getDescription().equals("im Garten")) {
			invent.addItem(schaufel);
		}
		
		//N�chster Raum wird gesetzt
		currentRoom = nextRoom;
		
		// wenn er die Richtige kombination und den Schl�ssel hat gewinnt er
		if (currentRoom.getDescription() == "im Labor" && win == true && invent.getItems().contains(schluessel)) {
			System.out.println("Sie haben die Richtige Kombination gefunden und haben damit gewonnen.");
			wantToQuit = true;
		}
		
		System.out.println("You are " + currentRoom.getDescription());
		System.out.print("Exits: ");

		for (i = 0; i < currentRoom.exits.size(); i++) {
			System.out.print(currentRoom.exits.get(i).getName() + " ");
		}
	}

	/**
	 * "Quit" was entered. Check the rest of the command to see whether we really
	 * quit the game. Return true, if this command quits the game, false otherwise.
	 * 
	 * @param command der eingegebene Befehl
	 * @return Gibt zur�ck ob man das Spiel wirklich verlassen will.
	 */
	private boolean quit(Command command) {
		if (command.hasSecondWord()) {
			System.out.println("Quit what?");
			return false;
		} else
			return true; // signal that we want to quit
	}
}
