package ch.bbw;

/**
 * Ist die Klasse f�r Gegenst�nde die sich auf der Map befinden.
 * 
 * @author jfr
 * @version 1.0
 *
 */
public class Item {
	private String name;

	/**
	 * Konstruktor der Klasse Item
	 * 
	 * @param name der Name des Items
	 */
	public Item(String name) {
		this.name = name;
	}

	/**
	 * Gibt den Namen des Items zur�ck
	 * 
	 * @return Name der Items
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt den Namen des Items
	 * 
	 * @param name  der Name des Items
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	
}
