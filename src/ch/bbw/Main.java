package ch.bbw;

/**
 * Main Klasse
 * 
 * @author jfr
 * @version 1.0
 *
 */
public class Main {

	/**
	 * Es wird eine neue Instanz der Klasse Game erstellt.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Game game = new Game();
		game.play();

	}

}
