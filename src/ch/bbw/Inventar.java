package ch.bbw;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Ist die Klasse f�r das Inventar des Spielers.
 * 
 * @author jfr
 *@version 1.0
 */
public class Inventar {
	private List<Item> item;

	/**
	 * Konstruktor der Klasse Inventar
	 * 
	 * @param item Name der Items 
	 */
	public Inventar(Item item) {
		this.item = new ArrayList<>(Arrays.asList(item));
	}
	
	/**
	 * Konstruktor der Klasse Inventar
	 * 
	 * @param items Name der Items
	 */
	public Inventar(ArrayList<Item> items) {
		this.item = items;
	}
	
	/**
	 * default Konstruktor
	 */
	public Inventar() {
		item = new ArrayList<>();
	}
	
	/**
	 * Gibt den Namen des Items zur�ck
	 * 
	 * @return Name des Items
	 */
	public List<Item> getItems() {
		return item;
	}

	/**
	 * Setzt den Namen des Items
	 *  
	 * @param item Name des Items
	 */
	public void addItem(Item item) {
		this.item.add(item);
	}
		
}
