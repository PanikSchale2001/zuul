package ch.bbw;

/**
 * erstellt alle Exits eines Raumes
 * 
 * @author jfr
 *@version 1.0
 */
public class Exit {
	private String name;
	private Room nextRoom;

	/**
	 * Der Konstruktor der Klasse Exit
	 * 
	 * @param name Name des Exits
	 * @param nextRoom n�chster Exit nach der T�r.
	 */
	public Exit(String name, Room nextRoom) {
		this.name = name;
		this.nextRoom = nextRoom;
	}
	
	/**
	 * default Konstruktor
	 */
	public Exit() {
		name = "Bad";
	}

	/**
	 * Gibt den Namen des Ausganges zur�ck
	 * 
	 * @return Gibt den Namen des Ausganes zur�ck
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setzt den Namen des Ausganges mit dem Wert des Parameters
	 * 
	 * @param name Der Name des Ausganges
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gibt den Namen des n�chsten Ausganges zur�ck
	 * 
	 * @return gibt den n�chsten Namen zur�ck
	 */
	public Room getNextRoom() {
		return nextRoom;
	}

	/**
	 * Setzt den Namen des Raumes nach dem Ausgang mit dem Wert des Parameters
	 * 
	 * @param nextRoom Der name des n�chsten Raum
	 */
	public void setNextRoom(Room nextRoom) {
		this.nextRoom = nextRoom;
	}
}
