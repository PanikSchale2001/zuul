package ch.bbw;

import java.util.ArrayList;

/*
 * Class Room - a room in an adventure game.
 *
 * This class is the main class of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  The exits are labelled north, 
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 * 
 * @author  jfr
 * @version 2.0 (Oktober 2018)
 */

public class Room {
	private String description;
	public ArrayList<Exit> exits = new ArrayList<>();
	public ArrayList<Item> item = new ArrayList<>();

	/**
	 * Create a room described "description". Initially, it has no exits.
	 * "description" is something like "a kitchen" or "an open court yard".
	 * 
	 * @param description die Beschreibung des Raumes zur�ck.
	 */
	public Room(String description) {
		this.description = description;
	}

	/**
	 * Return the description of the room (the one that was defined in the
	 * constructor).
	 * 
	 * @return Gibt die Beschreibung zur�ck.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Erstellt eine ArrayList f�r alle Ausg�ng
	 * 
	 * @param exit Name des Ausganges
	 */
	public void addExit(Exit exit) {
		exits.add(exit);

	}
	
	/**
	 * Generiert die Items
	 * 
	 * @param itemFromRoom Item welchers im Raum liegt
	 */
	public void addItem(Item itemFromRoom) {
		item.add(itemFromRoom);
	}
}
